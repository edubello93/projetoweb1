<?php
  $servername = "localhost";
  $username = "root";
  $password = "";
  $dbname = "forum";

  // Create connection
  $conn = mysqli_connect($servername, $username, $password);
  // Check connection
  if (!$conn) {
      die("Connection failed: " . mysqli_connect_error());
  }

  // Create database
  $sql = "CREATE DATABASE IF NOT EXISTS $dbname";
  if (mysqli_query($conn, $sql)) {
      echo "Database created successfully<br>";
  } else {
      echo "Error creating database: " . mysqli_error($conn);
  }
  mysqli_select_db($conn, "forum");

//tabela usuario, o login do usuario é feito com base no seu email
$sql = "CREATE TABLE IF NOT EXISTS user(
  id INT(6) AUTO_INCREMENT PRIMARY KEY,
  nick VARCHAR(15) NOT NULL,
  email VARCHAR(50) NOT NULL,
  senha VARCHAR(50) NOT NULL,
  reg_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE (email)
)";


if (mysqli_query($conn, $sql)) {
  echo "Table user created successfully<br>";
} else {
  echo "Error creating table: " . mysqli_error($conn);
}

//tabela topico, cada topico pode ter uma postagem
$sql = "CREATE TABLE IF NOT EXISTS topico(
  id INT(10) AUTO_INCREMENT PRIMARY KEY,
  titulo VARCHAR(60) NOT NULL,
  postagem VARCHAR(1000),
  criador INT(6),
  reg_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  CONSTRAINT FK_UserTopico FOREIGN KEY (criador) REFERENCES user(id)
)";
if (mysqli_query($conn, $sql)) {
  echo "Table topico created successfully<br>";
} else {
  echo "Error creating table: " . mysqli_error($conn);
}
//  mysqli_close($conn);


//tabela respostas, cada resposta referencia o usuario que a fez
//e o topico em que ela foi postada
$sql = "CREATE TABLE IF NOT EXISTS resposta(
  id INT (6) AUTO_INCREMENT PRIMARY KEY,
  conteudo VARCHAR (1000) NOT NULL,
  criador int(6),
  topicoId INT(10),
  reg_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  CONSTRAINT FK_UserResposta FOREIGN KEY (criador) REFERENCES user(id),
  CONSTRAINT FK_TopicoResposta FOREIGN KEY (topicoId) REFERENCES topico(id)
)";



if (mysqli_query($conn, $sql)) {
  echo "Table RESPOSTAS created successfully <br>";
} else {
  echo "Error creating table: " . mysqli_error($conn);
}

//insere exemplos de usuarios e topicos
//caso as tabelas estejam vazias
$query = "SELECT * from user";
$res = mysqli_query($conn, $query);
if (mysqli_fetch_array($res) == null) {
include 'exemplo_inserts.php';
}

  mysqli_close($conn);

?>
